package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func NewRouter() chi.Router {
	router := chi.NewRouter()
	router.Use(middleware.StripSlashes)

	routeTech(router)
	return router
}

func routeTech(router chi.Router) {
	router.Method(http.MethodGet, "/metrics", promhttp.Handler())
}
