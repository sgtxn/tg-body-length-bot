package listener

import (
	"tg-body-length-bot/pkg/prometheus"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/jimlawless/whereami"
	"github.com/pkg/errors"
)

func New(processor Processor, botToken string, chatID int64, pollInterval int, lg Logger) (*BotService, error) {
	svc := &BotService{
		pollInterval:   pollInterval,
		responseChatID: chatID,
		log:            lg,
		processor:      processor,
	}

	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		return nil, errors.Wrap(err, whereami.WhereAmI())
	}

	svc.account = bot.Self.UserName
	svc.bot = bot

	return svc, nil
}

func (svc *BotService) Start() {
	svc.log.Info("Started bot on account " + svc.account)

	updCfg := tgbotapi.NewUpdate(0)
	updCfg.Timeout = svc.pollInterval

	updChan, err := svc.bot.GetUpdatesChan(updCfg)
	if err != nil {
		svc.log.Fatal(errors.Wrap(err, whereami.WhereAmI()))
	}

	source := "Process update"

	for update := range updChan {
		if update.Message == nil || update.Message.Chat == nil {
			svc.log.Error("Got an empty message")
			prometheus.CountFailedAction(source, errors.Cause(err).Error())
			continue
		}

		resultText, err := svc.processUpdate(update)
		if err != nil {
			svc.log.Error(err)
			resultText = errors.Cause(err).Error()
		}

		chatID := svc.responseChatID
		if chatID < 1 {
			chatID = update.Message.Chat.ID
		}

		if err := svc.sendMessage(chatID, resultText); err != nil {
			svc.log.Error(err)
			prometheus.CountFailedAction(source, errors.Cause(err).Error())
			continue
		}

		prometheus.CountOKAction(source)
	}
}

func (svc *BotService) processUpdate(update tgbotapi.Update) (string, error) {
	defer prometheus.WriteActionTime(time.Now(), "Bot.ProcessUpdate")

	if update.Message.IsCommand() {
		return svc.processor.ProcessCommand(
			update.Message.Command(),
			update.Message.CommandArguments(),
			update.Message.Chat.ID,
		)
	}

	if update.Message.Document != nil {
		fileURL, err := svc.bot.GetFileDirectURL(update.Message.Document.FileID)
		if err != nil {
			return "", errors.Wrap(err, whereami.WhereAmI())
		}
		return svc.processor.ProcessDocument(fileURL, update.Message.Chat.ID)
	}

	err := errors.New("Couldn't find a way to process this message")
	return "", errors.Wrap(err, whereami.WhereAmI())
}

func (svc *BotService) sendMessage(chatID int64, msgText string) error {
	msg := tgbotapi.NewMessage(chatID, msgText)
	if _, err := svc.bot.Send(msg); err != nil {
		return err
	}
	return nil
}
