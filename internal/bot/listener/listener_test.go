package listener

import (
	"testing"
	"tg-body-length-bot/internal/bot/listener/mocks"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
)

func TestProcessUpdate_ShouldSucceed_OnCommand(t *testing.T) {
	cmdEntity := "bot_command"
	cmd := "@test"
	args := "1"
	chatID := int64(1)
	expectedResult := "OK"

	upd := tgbotapi.Update{
		Message: &tgbotapi.Message{
			Entities: &[]tgbotapi.MessageEntity{{Type: cmdEntity, Length: len(cmd)}},
			Text:     cmd + " " + args,
			Chat:     &tgbotapi.Chat{ID: chatID},
		},
	}
	proc := &mocks.Processor{}
	proc.On("ProcessCommand", "test", "1", chatID).Return(expectedResult, nil)

	bot := BotService{processor: proc}
	result, err := bot.processUpdate(upd)
	if err != nil {
		t.Fatalf("Didn't expect an error: %s", err.Error())
	}

	if result != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, result)
	}
}

func TestProcessUpdate_ShouldSucceed_OnOkDocument(t *testing.T) {
	fileID := "fileID"
	fileURL := "https://google.com"
	chatID := int64(1)
	expectedResult := "OK"

	upd := tgbotapi.Update{
		Message: &tgbotapi.Message{
			Document: &tgbotapi.Document{FileID: fileID},
			Chat:     &tgbotapi.Chat{ID: chatID},
		},
	}

	proc := &mocks.Processor{}
	bot := &mocks.Bot{}
	bot.On("GetFileDirectURL", fileID).Return(fileURL, nil)
	proc.On("ProcessDocument", fileURL, chatID).Return(expectedResult, nil)

	svc := BotService{processor: proc, bot: bot}
	result, err := svc.processUpdate(upd)
	if err != nil {
		t.Fatalf("Didn't expect an error: %s", err.Error())
	}

	if result != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, result)
	}
}

func TestProcessUpdate_ShouldFail_OnBadDocument(t *testing.T) {
	fileID := "fileID"
	chatID := int64(1)

	upd := tgbotapi.Update{
		Message: &tgbotapi.Message{
			Document: &tgbotapi.Document{FileID: fileID},
			Chat:     &tgbotapi.Chat{ID: chatID},
		},
	}

	bot := &mocks.Bot{}
	testErr := errors.New("test")
	bot.On("GetFileDirectURL", fileID).Return("", testErr).Once()
	svc := BotService{bot: bot}
	result, err := svc.processUpdate(upd)
	if err == nil {
		t.Fatalf("Expected an error here, but got %s", result)
	}
	if errors.Cause(err).Error() != testErr.Error() {
		t.Fatalf("Expected a different error here, but got %s", err.Error())
	}
}

func TestProcessUpdate_ShouldFail_OnBadMessage(t *testing.T) {
	chatID := int64(1)

	upd := tgbotapi.Update{
		Message: &tgbotapi.Message{
			Chat: &tgbotapi.Chat{ID: chatID},
		},
	}
	svc := BotService{}
	result, err := svc.processUpdate(upd)
	if err == nil {
		t.Fatalf("Expected an error here, but got %s", result)
	}
	if errors.Cause(err).Error() != "Couldn't find a way to process this message" {
		t.Fatalf("Expected a different error here, but got %s", err.Error())
	}
}

func TestSendMessage_ShouldSucceed_OnOkSend(t *testing.T) {
	chatID := int64(1)
	msgText := "test"
	bot := &mocks.Bot{}
	bot.On("Send", mock.Anything).Return(tgbotapi.Message{}, nil)
	svc := BotService{bot: bot}

	err := svc.sendMessage(chatID, msgText)
	if err != nil {
		t.Fatal("Expected an error here")
	}
}

func TestSendMessage_ShouldFail_OnSendError(t *testing.T) {
	chatID := int64(1)
	msgText := "test"
	bot := &mocks.Bot{}
	sendErr := errors.New("test err")
	bot.On("Send", mock.Anything).Return(tgbotapi.Message{}, sendErr)
	svc := BotService{bot: bot}

	err := svc.sendMessage(chatID, msgText)
	if err == nil {
		t.Fatal("Expected an error here")
	}
	if err != sendErr {
		t.Fatalf("Expected a different error here, but got %s", err.Error())
	}
}
