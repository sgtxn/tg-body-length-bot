package listener

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type Bot interface {
	GetUpdatesChan(config tgbotapi.UpdateConfig) (tgbotapi.UpdatesChannel, error)
	GetFileDirectURL(fileID string) (string, error)
	Send(c tgbotapi.Chattable) (tgbotapi.Message, error)
}

type Processor interface {
	ProcessCommand(cmd string, args string, chatID int64) (string, error)
	ProcessDocument(fileURL string, chatID int64) (string, error)
}

type Logger interface {
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
}

type BotService struct {
	bot            Bot
	responseChatID int64
	pollInterval   int
	log            Logger
	processor      Processor
	account        string
}
