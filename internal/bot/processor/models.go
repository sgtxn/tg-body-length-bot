package processor

type Requester interface {
	GetBody(url string) ([]byte, error)
}

type Processor struct {
	userThreadsMap map[int64]uint8
	requester      Requester
}
