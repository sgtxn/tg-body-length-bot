package processor

import (
	"fmt"
	"strconv"
	"testing"
	"tg-body-length-bot/internal/bot/processor/mocks"
	"time"
)

func TestProcessCommand_ShouldSucceed_OnOkCmdAndArgs(t *testing.T) {
	proc := New(time.Second * 10)

	threadsCount := uint8(8)
	chatID := int64(1)

	result, err := proc.ProcessCommand(setThreadsCmd, strconv.FormatInt(int64(threadsCount), 10), chatID)
	if err != nil {
		t.Fatalf("Didn't expect an error here: %s", err.Error())
	}

	expectation := fmt.Sprintf("Set threads count to %d", threadsCount)
	if result != expectation {
		t.Fatalf("Expected '%s' but got '%s'", expectation, result)
	}

	if count, ok := proc.userThreadsMap[chatID]; !ok || count != threadsCount {
		t.Fatalf("Expected '%s' but got '%s'", expectation, result)
	}
}

func TestProcessCommand_ShouldFail_OnOkCmdAndBadArgs(t *testing.T) {
	proc := New(time.Second * 10)

	threadsCount := uint8(0)
	chatID := int64(1)

	result, err := proc.ProcessCommand(setThreadsCmd, strconv.FormatInt(int64(threadsCount), 10), chatID)
	if err == nil {
		t.Fatalf("Expected an error here; result: %s", result)
	}

	result, err = proc.ProcessCommand(setThreadsCmd, "non-integer args", chatID)
	if err == nil {
		t.Fatalf("Expected an error here; result: %s", result)
	}
}

func TestProcessCommand_ShouldFail_OnUnknownCommand(t *testing.T) {
	proc := New(time.Second * 10)

	threadsCount := uint8(0)
	chatID := int64(1)

	result, err := proc.ProcessCommand("badcmd", strconv.FormatInt(int64(threadsCount), 10), chatID)
	if err == nil {
		t.Fatalf("Expected an error here; result: %s", result)
	}
}

func TestProcessDocument_ShouldSucceed_OnOkDocument(t *testing.T) {
	url := "https://google.com"
	urlList := fmt.Sprintf("%s\n%s\n%s", url, url, url)
	urlResultBody := "test body"
	docUrl := "docUrl"
	chatID := int64(1)

	rq := &mocks.Requester{}
	rq.On("GetBody", docUrl).Return([]byte(urlList), nil).Once()
	rq.On("GetBody", "https://google.com").Return([]byte(urlResultBody), nil)

	proc := Processor{
		userThreadsMap: make(map[int64]uint8),
		requester:      rq,
	}

	result, err := proc.ProcessDocument(docUrl, chatID)
	if err != nil {
		t.Fatalf("Didn't expect an error here: %s", err.Error())
	}

	length := len(urlResultBody)
	expectedResult := fmt.Sprintf("%s - %d\n%s - %d\n%s - %d\n",
		url, length, url, length, url, length)

	if result != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, result)
	}
}

func TestProcessDocument_ShouldSucceed_OnDocumentWithBadURLs(t *testing.T) {
	url := "https://google.com"
	badUrl := "not-a-url"
	urlList := fmt.Sprintf("%s\n%s\n%s", url, url, badUrl)
	urlResultBody := "test body"
	docUrl := "docUrl"
	chatID := int64(1)

	rq := &mocks.Requester{}
	rq.On("GetBody", docUrl).Return([]byte(urlList), nil).Once()
	rq.On("GetBody", "https://google.com").Return([]byte(urlResultBody), nil)

	proc := Processor{
		userThreadsMap: make(map[int64]uint8),
		requester:      rq,
	}

	result, err := proc.ProcessDocument(docUrl, chatID)
	if err != nil {
		t.Fatalf("Didn't expect an error here: %s", err.Error())
	}

	length := len(urlResultBody)
	expectedResult := fmt.Sprintf("%s - %s\n%s - %d\n%s - %d\n",
		badUrl, "Bad URL", url, length, url, length)

	if result != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, result)
	}
}
