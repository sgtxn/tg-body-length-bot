package processor

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"tg-body-length-bot/pkg/prometheus"
	"tg-body-length-bot/pkg/requester"
	"time"

	"github.com/jimlawless/whereami"
	"github.com/pkg/errors"
)

const maxUint8 = 1<<8 - 1

const (
	setThreadsCmd = "set_threads"
	helpCmd       = "help"

	helpString = `Set maximum number of concurrent requests: 
	/set_threads <num>
Supported <num> range: 1 to 255

Display help: 
	/help
`
)

func New(httpTimeout time.Duration) *Processor {
	return &Processor{
		userThreadsMap: make(map[int64]uint8),
		requester:      requester.New(httpTimeout),
	}
}

func (proc *Processor) ProcessCommand(cmd string, args string, chatID int64) (string, error) {
	switch cmd {
	case setThreadsCmd:
		threadsNum, err := strconv.ParseInt(args, 10, 64)
		if err != nil {
			return "", errors.Wrap(err, whereami.WhereAmI())
		}
		if threadsNum < 1 || threadsNum > maxUint8 {
			err = fmt.Errorf("Value %d is out of bounds; see /%s for more info", threadsNum, helpCmd)
			return "", errors.Wrap(err, whereami.WhereAmI())
		}
		proc.userThreadsMap[chatID] = uint8(threadsNum)
		return fmt.Sprintf("Set threads count to %d", threadsNum), nil
	case helpCmd:
		return helpString, nil
	default:
		err := errors.New("Unknown command")
		return "", errors.Wrap(err, whereami.WhereAmI())
	}
}

func (proc *Processor) ProcessDocument(fileURL string, chatID int64) (string, error) {
	maxThreads, ok := proc.userThreadsMap[chatID]
	if !ok {
		maxThreads = 2
	}

	body, err := proc.requester.GetBody(fileURL)
	if err != nil {
		return "", errors.Wrap(err, whereami.WhereAmI())
	}

	var result strings.Builder
	okURLs, badURLs := getURLArrayFromString(string(body))

	for _, link := range badURLs {
		result.WriteString(link + " - Bad URL\n")
	}

	urlCh := make(chan string)
	resultCh := make(chan string)
	doneCh := make(chan struct{})

	wg := &sync.WaitGroup{}
	wg.Add(len(okURLs))

	// Launching worker threads, but no more than the number of urls we need to process
	for i := 0; i < int(maxThreads) && i < len(okURLs); i++ {
		go proc.getHttpWorker(urlCh, resultCh, wg)
	}

	// A thread feeding incoming data to workers
	go func() {
		for _, url := range okURLs {
			urlCh <- strings.TrimSpace(url)
		}
		close(urlCh)
	}()

	// A thread waiting for all workers to return, in order to evade write-to-closed-channel panics
	go func() {
		wg.Wait()
		close(doneCh)
	}()

	for {
		select {
		case str := <-resultCh:
			result.WriteString(str)
		case <-doneCh:
			return result.String(), nil
		}
	}
}

func (svc *Processor) getHttpWorker(urlCh <-chan string, resultCh chan<- string, wg *sync.WaitGroup) {
	for url := range urlCh {
		body, err := svc.requester.GetBody(url)
		if err != nil {
			resultCh <- fmt.Sprintf("%s - %s: %s\n", url, "Request Error", err.Error())
			prometheus.CountFailedAction("Request", err.Error())
			wg.Done()
			continue
		}

		prometheus.CountOKAction("Request")
		resultCh <- fmt.Sprintf("%s - %d\n", url, len(body))
		wg.Done()
	}
}

func getURLArrayFromString(urlStr string) (okResult []string, badResult []string) {
	links := strings.Split(string(urlStr), "\n")

	for _, link := range links {
		if _, err := url.ParseRequestURI(link); err == nil {
			okResult = append(okResult, link)
			continue
		}
		badResult = append(badResult, link)
	}

	return
}
