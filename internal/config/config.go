package config

import (
	"flag"

	"github.com/BurntSushi/toml"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	TelegramPollInterval int
	RequestTimeout       int
	BotToken             string `split_words:"true"`
	ResponseChatID       int64  `default:"-1" split_words:"true"`
}

func Init() *Config {
	// Flag to simplify debugging
	cfgPath := flag.String("c", "configs/config.toml", "config path")
	flag.Parse()

	var cfg Config
	if _, err := toml.DecodeFile(*cfgPath, &cfg); err != nil {
		panic(err)
	}

	if err := envconfig.Process("", &cfg); err != nil {
		panic(err)
	}

	return &cfg
}
