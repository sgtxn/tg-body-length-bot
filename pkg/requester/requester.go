package requester

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"tg-body-length-bot/pkg/prometheus"
	"time"

	"github.com/jimlawless/whereami"
	"github.com/pkg/errors"
)

type Requester struct {
	client *http.Client
}

func New(timeout time.Duration) *Requester {
	return &Requester{
		client: &http.Client{Timeout: timeout},
	}
}

func (r *Requester) GetBody(url string) ([]byte, error) {
	defer prometheus.WriteActionTime(time.Now(), "Requester.GetBody")

	resp, err := r.client.Get(url)
	if err != nil {
		return nil, errors.Wrap(err, whereami.WhereAmI())
	}

	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusMultipleChoices {
		err = fmt.Errorf("Got a response with non-2xx status code: %d", resp.StatusCode)
		return nil, errors.Wrap(err, whereami.WhereAmI())
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, whereami.WhereAmI())
	}

	return respBody, nil
}
