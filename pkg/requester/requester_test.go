package requester

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	response = "OK Response"
)

func TestRequester_ShouldSucceed_OnOkResponse(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(response))
	})

	srv := httptest.NewServer(handler)
	defer srv.Close()
	rq := Requester{client: srv.Client()}

	body, err := rq.GetBody(srv.URL)
	if err != nil {
		t.Errorf("Got an error: %s", err.Error())
	}

	if string(body) != response {
		t.Errorf("Expected '%s' but got '%s'", response, body)
	}
}

func TestRequester_ShouldFail_OnBadStatusCode(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(response))
	})

	srv := httptest.NewServer(handler)
	defer srv.Close()
	rq := Requester{client: srv.Client()}

	_, err := rq.GetBody(srv.URL)
	if err == nil {
		t.Errorf("Got an error: %s", err.Error())
	}
}

func TestRequester_ShouldFail_OnBadBodyRead(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Length", "1")
		w.WriteHeader(http.StatusOK)
	})

	srv := httptest.NewServer(handler)
	defer srv.Close()
	rq := Requester{client: srv.Client()}

	_, err := rq.GetBody(srv.URL)
	if err == nil {
		t.Errorf("Got an error: %s", err.Error())
	}
}
