module tg-body-length-bot

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-chi/chi v1.5.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jimlawless/whereami v0.0.0-20160417220522-aebf70d4a772
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.8.0
	github.com/stretchr/testify v1.4.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	go.uber.org/zap v1.13.0
)
