# tg-body-length-bot

A simple telegram bot accepting a list of URLs and returning the length of each GET response body.  

## Environment variables

* BOT_TOKEN - A token you're supposed to receive from BotFather telegram bot
* RESPONSE_CHAT_ID - You might want to set it in case you want to redirect bot's answers to somewhere else other than the chat with the user. Otherwise leave empty.

## Running in docker  
```docker run -p 8080:8080 --env BOT_TOKEN="<BOT_TOKEN>" --env RESPONSE_CHAT_ID=<CHAT_ID> registry.gitlab.com/sgtxn/tg-body-length-bot/image:1.0.0-prod```

## Metrics server
Accessible at http://localhost:8080/metrics