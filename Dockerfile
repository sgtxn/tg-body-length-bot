FROM alpine:3.12 as alpine
RUN apk --no-cache add ca-certificates && apk --no-cache add tzdata
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && echo "Europe/Moscow" > /etc/timezone

FROM scratch
EXPOSE 8080
COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=alpine /etc/localtime /etc/localtime
COPY --from=alpine /etc/timezone /etc/timezone
ADD tg-body-length-bot /
ADD configs /configs

ENTRYPOINT ["./tg-body-length-bot"]%                                          
