package main

import (
	"net/http"
	"tg-body-length-bot/internal/bot/listener"
	"tg-body-length-bot/internal/bot/processor"
	"tg-body-length-bot/internal/config"
	"tg-body-length-bot/internal/router"
	"tg-body-length-bot/pkg/logger"
	"tg-body-length-bot/pkg/prometheus"
	"time"
)

func init() {
	prometheus.Init()
}

func main() {
	cfg := config.Init()
	lg := logger.New()
	httpTimeout := time.Duration(cfg.RequestTimeout) * time.Second

	prc := processor.New(httpTimeout)
	bot, err := listener.New(prc, cfg.BotToken, cfg.ResponseChatID, cfg.TelegramPollInterval, lg)
	if err != nil {
		lg.Fatal(err)
	}

	go bot.Start()

	server := http.Server{
		Addr:         ":8080",
		Handler:      router.NewRouter(),
		ReadTimeout:  httpTimeout,
		WriteTimeout: httpTimeout,
	}

	lg.Info("Server is up and running")
	lg.Info(server.ListenAndServe().Error())
}
